﻿using EasyNetQ;
using Library;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sub
{
    class Program
    {
        public static object Locker = new object();
        public static int count = 0;
        static void Main(string[] args)
        {
            try
            {
                int count = 0;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                var advancedBus = RabbitHutch.CreateBus(AppSetting.RebbitMQConntection).Advanced;

                var queue = advancedBus.QueueDeclare("my.queue8", durable: true, expires: 1000 * 30 * 60);
                advancedBus.Consume(queue, (body, properties, info) => Task.Factory.StartNew(() =>
                {
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine("Got message: '{0}'", message);

                    count++;
                    if (count == 1000)
                    {
                        stopWatch.Stop();
                        TimeSpan ts = stopWatch.Elapsed;
                        Console.WriteLine(ts.TotalSeconds);
                    }

                    //Thread.Sleep(1000);
                }));

                Console.WriteLine("Sub my.queue");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
