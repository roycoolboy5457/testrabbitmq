﻿using EasyNetQ;
using EasyNetQ.Topology;
using Library;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pub
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IAdvancedBus advancedBus = RabbitHutch.CreateBus(AppSetting.RebbitMQConntection).Advanced;
                var exchange = advancedBus.ExchangeDeclare("my.exchange123", ExchangeType.Fanout);
                var queue = advancedBus.QueueDeclare("my.queue8", durable: true, expires: 1000 * 30 * 60);
                //var queue = advancedBus.QueueDeclare("my.queue3", durable: true);
                var binding = advancedBus.Bind(exchange, queue, "");

                var properties = new MessageProperties()
                {
                    DeliveryMode = 2
                };

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                for (long i = 1; i <= 1000000000; i++)
                {
                    var message = "Id:" + i;

                    var messageByte = Encoding.UTF8.GetBytes(message);
                    advancedBus.PublishAsync(exchange, "", false, properties, messageByte).ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            Console.WriteLine("publish confirm loss");
                        }
                    });
                    Console.WriteLine("Pub Message" + message);
                    //Thread.Sleep(10);
                }
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                Console.WriteLine(ts.TotalSeconds);

                Console.WriteLine("wait...");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
    }
}
