﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyNetQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var advancedBus = RabbitHutch.CreateBus(AppSetting.RebbitMQConntection).Advanced;

            var queue = advancedBus.QueueDeclare("my.queue2");
            advancedBus.Consume(queue, (body, properties, info) => Task.Factory.StartNew(() =>
            {
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Got message: '{0}'", message);
            }));

            Console.WriteLine("Sub a Message");
        }
    }
}
