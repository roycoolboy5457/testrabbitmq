﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sub2
{
    class Program
    {
        static void Main(string[] args)
        {
            var advancedBus = RabbitHutch.CreateBus(AppSetting.RebbitMQConntection).Advanced;
            //var advancedBus = RabbitHutch.CreateBus("host=10.10.104.135;username=admin;password=yaudian!@#;publisherConfirms=true;timeout=10").Advanced;
            var queue = advancedBus.QueueDeclare("my.queue2");
            advancedBus.Consume(queue, (body, properties, info) => Task.Factory.StartNew(() =>
            {
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Got message: '{0}'", message);
            }));

            Console.WriteLine("Sub my.queue2");
        }
    }
}
