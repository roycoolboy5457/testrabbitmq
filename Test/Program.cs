﻿using EasyNetQ;
using Library;
using RabbitMQ.Client;
using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var advancedBus = RabbitHutch.CreateBus(AppSetting.RebbitMQConntection).Advanced;
            var exchange = advancedBus.ExchangeDeclare("my.exchange123", ExchangeType.Fanout);
            var queue = advancedBus.QueueDeclare("my.queue3",durable:false,expires: 1000 * 60 * 30);
            var binding = advancedBus.Bind(exchange, queue, "");

            Console.WriteLine("Hello World!");
        }
    }
}
